# List of free and open source Android software

## Advertisement blocking

Application Name    |   Description |   Root    |   Availability    |   Source Code |
:---------: | :-----------: | :---------------: | :---------------: | :-----------: |
|[AdAway](https://adaway.org/)   |   Ad blocker for Android using the hosts file.    |   Yes |  <a href="https://www.f-droid.org/en/packages/org.adaway/"> <img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="70"> |   <a href="https://github.com/AdAway/AdAway" a> <img src="https://cdn.afterdawn.fi/v3/news/original/github-logo.png" height="70">
|[Blokada](https://blokada.org/)    |   Ad blocker for Android using the VPN API.   |   No  |   <a href="https://f-droid.org/en/packages/org.blokada.alarm/"> <img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="70">   |  <a href="https://github.com/blokadaorg/blokada" a> <img src="https://cdn.afterdawn.fi/v3/news/original/github-logo.png" height="70">

## Browsers

Application Name    |   Description |   Root    |   Availability    |   Source Code |
:---------: | :-----------: | :---------------: | :---------------: | :-----------: |
|[Kiwi Browser](https://kiwibrowser.com/)   |   Based on Chrome Canary; extension support; built-in dark mode; degoogled.    |   No |   <a href="https://play.google.com/store/apps/details?id=com.kiwibrowser.browser&hl=en_US"> <img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" height="70" alt="Get it on Google Play">    |   <a href="https://github.com/kiwibrowser"> <img src="https://cdn.afterdawn.fi/v3/news/original/github-logo.png" height="70">
|[Bromite](https://www.bromite.org/)    |   Based on Chromium; built-in adblocking; enhanced privacy; degoogled. |   No  |   <a href="https://github.com/bromite/bromite/releases"> <img src="https://www.bromite.org/bromite.png" alt="Bromite" height="70">    |   <a href="https://github.com/bromite/bromite"> <img src="https://cdn.afterdawn.fi/v3/news/original/github-logo.png" height="70">

## Stores

Application Name    |   Description |   Root    |   Availability    |   Source Code |
:---------: | :-----------: | :---------------: | :---------------: | :-----------: |
|Aurora Droid   |   Fork of F-Droid client with external repos ready to sync.   |   Optional  |   <a href="https://f-droid.org/en/packages/com.aurora.adroid/"> <img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="70"> |   <a href="https://gitlab.com/AuroraOSS/auroradroid"> <img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="70">
|[Aurora Store](https://auroraoss.com/) |   Unofficial FOSS client to Google Play Store. |   Optional   |   <a href="https://f-droid.org/en/packages/com.aurora.store/"> <img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="70">   |   <a href="https://gitlab.com/AuroraOSS/AuroraStore"> <img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="70">


## Personalisation

Application Name    |   Description |   Root    |   Availability    |   Source Code |
:---------: | :-----------: | :---------------: | :---------------: | :-----------: |
|[Lawnchair 2](https://lawnchair.app/)   |   Continuation of Lawnchair 1; Pixel features; fork of Launcher3. |   No  |   <a href="https://play.google.com/store/apps/details?id=ch.deletescape.lawnchair.plah&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"> <img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" height="70" alt="Get it on Google Play">  <a href="https://www.apkmirror.com/apk/deletescape/lawnchair/"> <img src="https://lawnchair.app/images/get-it-on-apkmirror.png" alt="Get it on APKMirror" height="70"> |   <a href="https://github.com/LawnchairLauncher/Lawnchair"> <img src="https://cdn.afterdawn.fi/v3/news/original/github-logo.png" height="70">
|Librechair |   Degoogled; fork of Lawnchair V2.    |   No  |   <a href="https://t.me/librechair"> <img src="https://telegram.org/img/t_logo.png" alt="Telegram" height="70">  |   <a href="https://gitlab.com/oldosfan/librechair"> <img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="70">

## External sources